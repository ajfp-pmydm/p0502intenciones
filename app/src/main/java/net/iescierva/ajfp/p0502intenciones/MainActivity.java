package net.iescierva.ajfp.p0502intenciones;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;

import android.Manifest;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[] { Manifest.permission.CALL_PHONE },
                    1);

        findViewById(R.id.butAbrirWeb).setOnClickListener(this);
        findViewById(R.id.butCorreo).setOnClickListener(this);
        findViewById(R.id.butFoto).setOnClickListener(this);
        findViewById(R.id.butLlamada).setOnClickListener(this);
        findViewById(R.id.butMaps).setOnClickListener(this);
        findViewById(R.id.butBuscar).setOnClickListener(this);
    }

    public void pgWeb(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.androidcurso.com/"));
        startActivity(intent);
    }

    public void llamadaTelefono(View view) {
        Intent intent = new Intent(Intent.ACTION_CALL,
                Uri.parse("tel:962849347"));
        startActivity(intent);
    }

    public void googleMaps(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("geo:41.656313,-0.877351"));
        startActivity(intent);
    }

    public void tomarFoto(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivity(intent);
    }

    public void mandarCorreo(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "asunto");
        intent.putExtra(Intent.EXTRA_TEXT, "texto del correo");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"miguelangel.ibanez@murciaeduca.es"});
        startActivity(intent);
    }

    public void buscaGoogle(View view) {
        Uri uri = Uri.parse("https://www.google.com/search?q=hey");
        Intent gSearchIntent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(gSearchIntent);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.butAbrirWeb:
                pgWeb(v);
                break;
            case R.id.butCorreo:
                mandarCorreo(v);
                break;
            case R.id.butMaps:
                googleMaps(v);
                break;
            case R.id.butFoto:
                tomarFoto(v);
                break;
            case R.id.butLlamada:
                llamadaTelefono(v);
                break;
            case R.id.butBuscar:
                buscaGoogle(v);
                break;
        }
    }
}